Source: fclib
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Stephen Sinclair <radarsat1@gmail.com>
Section: science
Priority: optional
Build-Depends: debhelper (>= 11),
               cmake (>= 3.0.2),
               libhdf5-dev (>= 1.10),
               libsuitesparse-dev (>= 1:5.1.2)
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/science-team/fclib
Vcs-Git: https://salsa.debian.org/science-team/fclib.git
Homepage: https://frictionalcontactlibrary.github.io/
Rules-Requires-Root: no

Package: libfclib0
Architecture: any
Multi-Arch: same
Section: libs
Depends: ${misc:Depends},
         ${shlibs:Depends}
Description: read and write problems from the Friction Contact Library (library)
 fclib is an open source collection of Frictional Contact (FC)
 problems stored in a specific HDF5 format, and an open source light
 implementation of Input/Output functions in C Language to read and
 write problems.
 .
 The goal of this work is to set up a collection of 2D and 3D
 Frictional Contact (FC) problems in order to set up a list of
 benchmarks; provide a standard framework for testing available and
 new algorithms; and share common formulations of problems in order to
 exchange data.
 .
 Fclib is an open-source scientific software primarily targeted at
 modeling and simulating nonsmooth dynamical systems
 .
 This package includes the libfclib libraries.

Package: libfclib-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends: ${misc:Depends},
         ${shlibs:Depends},
         libfclib0 (= ${binary:Version}),
         libhdf5-dev (>= 1.10),
         libsuitesparse-dev (>= 1:5.1.2)
Description: read and write problems from the Friction Contact Library (headers)
 fclib is an open source collection of Frictional Contact (FC)
 problems stored in a specific HDF5 format, and an open source light
 implementation of Input/Output functions in C Language to read and
 write problems.
 .
 The goal of this work is to set up a collection of 2D and 3D
 Frictional Contact (FC) problems in order to set up a list of
 benchmarks; provide a standard framework for testing available and
 new algorithms; and share common formulations of problems in order to
 exchange data.
 .
 Fclib is an open-source scientific software primarily targeted at
 modeling and simulating nonsmooth dynamical systems
 .
 This package includes the libfclib development headers.
